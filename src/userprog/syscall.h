#ifndef USERPROG_SYSCALL_H
#define USERPROG_SYSCALL_H

#include <list.h>
#include "threads/thread.h"
#include "filesys/file.h"

void syscall_init (void);

struct file_fd {
  struct file *file;       /* Corresponding file */
  int fd;                  /* Correpondning file descriptor */
  struct thread *process;  /* Process that owns this file */
  struct list_elem elem;   /* List element */
};


#endif /* userprog/syscall.h */
