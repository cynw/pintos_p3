#include "userprog/syscall.h"
#include <stdio.h>
#include <syscall-nr.h>
#include "threads/interrupt.h"
#include "threads/thread.h"
#include "threads/init.h"
#include "devices/input.h"
#include "userprog/pagedir.h"
#include "threads/vaddr.h"
#include "vm/page.h"

static uint32_t *esp;

static void syscall_handler (struct intr_frame *);
void exit (int status);
tid_t exec (const char *cmd_line);

int open (const char *file);
int filesize (int fd);
int read (int fd, void *buffer, unsigned size);
int wrtie (int fd, const void *buffer, unsigned size);
void seek (int fd, unsigned position);
unsigned tell (int fd);
void close (int fd);


static struct file *get_file_from_fd (int fd);
static struct file_fd *get_ff_from_fd (int fd);
static int fid = 2;
static bool is_valid_ptr (void *vaddr);
struct lock fs_lock;

void
syscall_init (void) 
{
  intr_register_int (0x30, 3, INTR_ON, syscall_handler, "syscall");
  lock_init(&fs_lock);
}

static void
syscall_handler (struct intr_frame *f UNUSED) 
{
  esp = f->esp;
  int *syscall_no = f->esp;
  int status;
  struct file *file;
  uint8_t c;

  if (!is_valid_ptr (esp)) 
    exit(-1);

  switch(*syscall_no) {
    case SYS_HALT:
      power_off();
      break;

    case SYS_EXIT:
      if (!is_valid_ptr (esp+1))
        exit(-1);
      f->eax = *(esp+1);
      exit(*(esp+1));  
      break;

    case SYS_EXEC:
      if (!is_valid_ptr (esp+1))
        exit(-1);
      if (!is_valid_ptr (*(esp+1)))
        exit(-1);

      f->eax = exec(*(esp+1));
      break;

    case SYS_WAIT:
      if (!is_valid_ptr (esp+1))
        exit(-1);
      f->eax = process_wait(*(esp+1));
      break;

    case SYS_CREATE:
      if (!is_valid_ptr (esp+1) || !is_valid_ptr (*(esp+1)))
        exit(-1);

      lock_acquire(&fs_lock);
      f->eax = filesys_create(*(esp+1), *(esp+2));
      lock_release(&fs_lock);
      break;

    case SYS_REMOVE:
      if (!is_valid_ptr (esp+1) || !is_valid_ptr (*(esp+1)))
        exit(-1);

      lock_acquire(&fs_lock);
      f->eax = filesys_remove(*(esp+1));
      lock_release(&fs_lock);
      break;

    case SYS_OPEN:
      if (!is_valid_ptr (esp+1) || !is_valid_ptr (*(esp+1)))
        exit(-1);

      f->eax = open(*(esp+1));
      break;

    case SYS_FILESIZE:
      if (!is_valid_ptr (esp+1))
        exit(-1);

      f->eax = filesize(*(esp+1));
      break;

    case SYS_READ:
      if (!is_valid_ptr (esp+1) || !is_valid_ptr (esp+2) || !is_valid_ptr (esp+3)
          || !is_valid_ptr (*(esp+2)))
        exit(-1);

      f->eax = read(*(esp+1), *(esp+2), *(esp+3));
      break;

    case SYS_WRITE:
      if (!is_valid_ptr (esp+1) || !is_valid_ptr (esp+2) || !is_valid_ptr(esp+3)
          || !is_valid_ptr (*(esp+2)))
        exit(-1);

      f->eax = write(*(esp+1), *(esp+2), *(esp+3));
      break;

    case SYS_SEEK:
      if (!is_valid_ptr (esp+1) || !is_valid_ptr (esp+2))
        exit(-1);

      seek(*(esp+1), *(esp+2));
      break;

    case SYS_TELL:
      if (!is_valid_ptr (esp+1))
        exit(-1);

      f->eax = tell(*(esp+1));
      break;

    case SYS_CLOSE:
      if (!is_valid_ptr (esp+1))
        exit(-1);

      close(*(esp+1));
      break;
  }
}

void exit(int status) {
  thread_current()->exit_status = status;
  thread_exit();
}

tid_t exec (const char *cmd_line) {
  sema_down(&thread_current()->child_load_sema);
  tid_t child_tid = process_execute(cmd_line);

  sema_down(&thread_current()->child_load_sema);
  if (thread_current()->child_load_status != 1)
    child_tid = TID_ERROR;
  sema_up(&thread_current()->child_load_sema);
  return child_tid;
}

int open (const char *file) {
  int open_fd = -1;

  lock_acquire(&fs_lock);
  struct file *f = filesys_open (file);
  struct file_fd *ff;
  if (f != NULL) {
    ff = calloc(1, sizeof *ff);
    ff->file = f;
    ff->fd = fid++;
    ff->process = thread_current();
    list_push_back(&thread_current()->files, &ff->elem);
    open_fd = ff->fd;
  }
  lock_release(&fs_lock);
  return open_fd;
}

int filesize (int fd) {
  if (fd > fid)
    exit(-1);

  int retval = 0;

  lock_acquire (&fs_lock);
  struct file *file = get_file_from_fd(fd);
  if (file == NULL)
    retval = -1;
  else
    retval = file_length(file);
  lock_release (&fs_lock);

  return retval;
}

int read (int fd, void *buffer, unsigned size) {
  if (fd > fid)
    exit(-1);

  int retval = 0;
  struct pte *page = page_lookup(buffer, &thread_current()->pages);
/*
  if (page == NULL || !page->writable){
      printf("read: not writable\n");
      exit(-1);
  }
*/

  lock_acquire (&fs_lock);
  if (fd == STDOUT_FILENO)
    retval = -1;

  else if (fd == STDIN_FILENO){
    uint8_t c = input_getc();
    memset(buffer, c, 1);
    retval = 1;
  }

  else {
    struct file *file = get_file_from_fd(fd);
    if (file == NULL) {
      retval = -1;
    }
    else
      retval = file_read (file, buffer, size);
  }
  lock_release (&fs_lock);

  return retval;
}

int write(int fd, const void *buffer, unsigned size) {
  if (fd > fid)
    exit(-1);

  int retval = 0;

  lock_acquire (&fs_lock);
  if (fd == STDOUT_FILENO) {
    putbuf(buffer, size);
    memset(buffer, 0, size);
    retval = size;
  }

  else if (fd == STDIN_FILENO)
    retval = -1;

  else {
    struct file *file = get_file_from_fd(fd);
    if (file == NULL)
      retval = -1;
    else 
      retval = file_write (file, buffer, size);
  }
  lock_release (&fs_lock);
    
  return retval;
}

void seek (int fd, unsigned position) {
  lock_acquire (&fs_lock);
  struct file *file = get_file_from_fd(fd);
  if (file != NULL)
    file_seek(file, position);
  lock_release (&fs_lock);
}

unsigned tell (int fd) {
  int retval = 0;

  lock_acquire (&fs_lock);
  struct file *file = get_file_from_fd(fd);
  if (file != NULL)
    retval = file_tell(file);
  lock_release (&fs_lock);
  return retval;
}

void close (int fd) {
  if (fd > fid)
    exit(-1);

  lock_acquire(&fs_lock);
  struct file *file = get_file_from_fd(fd);
  struct file_fd *ff = get_ff_from_fd(fd);
  
  if (file == NULL || ff == NULL) {
    lock_release(&fs_lock);
    return;
  }

  file_close(file);

  list_remove(&ff->elem);
  free(ff);
  lock_release(&fs_lock);
}


/* Returns a file descriptor from the assigned fid */
static struct file *
get_file_from_fd (int fd) {
  struct file_fd *ff = get_ff_from_fd (fd);
  if (ff == NULL)
    return NULL;

  return ff->file;
}

static struct file_fd *
get_ff_from_fd (int fd) {
  struct thread *curr = thread_current();
  struct list_elem *e;

  for (e = list_begin(&curr->files); e != list_end(&curr->files);
       e = list_next(e)) {
    struct file_fd *ff = list_entry(e, struct file_fd, elem);
    if (ff->fd == fd) {
      return ff;
    }
  }
  return NULL;
}


bool
is_valid_ptr (void *vaddr) {
  if (vaddr == NULL || !is_user_vaddr (vaddr)) {
    return false;
  }

  if (pagedir_get_page (thread_current()->pagedir, vaddr) == NULL)
    return false;
  return true;
}

