#include "vm/frame.h"
#include "threads/palloc.h"
#include "threads/vaddr.h"
#include "threads/synch.h"
#include "threads/thread.h"
#include "devices/timer.h"
#include "userprog/pagedir.h"
#include <list.h>

struct list frames;
static bool tick_less_func (const struct list_elem *a, const struct list_elem *b, void *aux);
struct lock frame_lock;

struct fte *get_victim();

void setup_frames()
{
  list_init (&frames);
  lock_init (&frame_lock);
}

/*void frame_evict(struct fte *f)
{
  struct pte *p = f->page;
  if(pagedir_is_dirty(f->owner->pagedir, p->addr)) {
    swap_in(f->page);
  }
  else
    if(f->page->file != NULL) {
    pagedir_clear_page(f->owner->pagedir, p->addr);
        p->location = PG_FILE;
    }
  /* Update frame table */
  /*list_remove(&f->list_elem);
  palloc_free_page(ptov(p->p_addr));
}*/

bool add_frame(struct pte *p, uintptr_t frame_addr)
{
  struct fte *fte = malloc(sizeof(struct fte));
  fte->page = p;
  fte->frame_addr = frame_addr;
  fte->last_access = timer_ticks();
  fte->owner = thread_current();
  
  lock_acquire(&frame_lock);
  list_insert_ordered (&frames, &fte->list_elem, tick_less_func, NULL);
  lock_release(&frame_lock);
  
  return true;
}
    
struct list *get_frames()
{
  return &frames;
}

struct fte *get_victim(){
  struct list_elem *e;
  int64_t min_access_time;
  struct fte *victim;

  lock_acquire(&frame_lock);
  
  struct fte *f = list_entry (list_begin(&frames), struct fte, list_elem);
  min_access_time = f->last_access;
  
  for (e = list_begin(&frames); e != list_end(&frames); e = list_next(e)) {
    struct fte *f = list_entry(e, struct fte, list_elem);
    if (f->page->swap && f->last_access < min_access_time) {
      victim = f;
      min_access_time = f->last_access;
    }
  }
  lock_release(&frame_lock);
  return f; 
}

void remove_frames(struct thread *t){
  struct list_elem *e, *temp;

  lock_acquire(&frame_lock);
/*  
  for (e = list_begin(&frames); e != list_end(&frames); e = list_next(e)) {
    struct fte *f = list_entry(e, struct fte, list_elem);
    if (f->owner_tid == tid) {
        printf("Removing frame\n");
        list_remove(e);
        free(f);
    }
  }
*/
  e = list_begin(&frames);
  while (e != list_end(&frames)) {
    struct fte *f = list_entry(e, struct fte, list_elem);
    temp = e; e = list_next(e);
    if (f->owner == t) {
      list_remove(temp);
      printf("Removing frame\n");
      free(f);
    }
  }

  lock_release(&frame_lock);
}

//TODO: when frame is accessed (?) --> pagedir_get_page (kpage) --> vtop (phy.addr)
//
void frame_set_access(uintptr_t pa)
{
  struct list_elem *e;
  for (e = list_begin (&frames); e != list_end (&frames);
       e = list_next (e))
  {
    struct fte *f = list_entry (e, struct fte, list_elem);
    if (f->frame_addr == pa) {
      list_remove(&f->list_elem);
      f->last_access = timer_ticks();
      list_insert_ordered (&frames, &f->list_elem, tick_less_func, NULL);
      return;
    }
  }
}

struct fte *get_fte_from_pte(struct pte *p)
{
  struct fte *fte;
  struct list_elem *e;

  lock_acquire(&frame_lock);
  for (e = list_begin (&frames); e != list_end (&frames);
       e = list_next (e))
  {
    struct fte *f = list_entry (e, struct fte, list_elem);
    if (f->frame_addr == p->p_addr) {
      fte = f;
      break;
    }
  }
  lock_release(&frame_lock);

  return fte;
}

struct fte *get_fte_from_frame(void *frame_addr)
{
  struct fte *fte;
  struct list_elem *e;

  lock_acquire(&frame_lock);
  for (e = list_begin (&frames); e != list_end (&frames);
       e = list_next (e))
  {
    struct fte *f = list_entry (e, struct fte, list_elem);
    if (f->frame_addr == frame_addr) {
      fte = f;
      lock_release(&frame_lock);
      return fte;
    }
  }
  printf("Notfound given fA=%x\n", frame_addr);
  lock_release(&frame_lock);
  return NULL;
}

/* Function comparing tick of last access returning bool */
static bool
tick_less_func (const struct list_elem *a, const struct list_elem *b, void *aux)
{
  const struct fte *a_ = list_entry (a, struct fte, list_elem);
  const struct fte *b_ = list_entry (b, struct fte, list_elem);

  return a_->last_access < b_->last_access;
}
