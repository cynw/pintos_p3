#include <hash.h>
#include "vm/page.h"
#include "threads/thread.h"

struct fte{
  struct pte *page;              /* Pointer to page */
  uintptr_t frame_addr;          /* Address to frame */
  struct thread *owner;          /* Owner thread */

  int64_t last_access;           /* Last time frame was accessed */
  struct list_elem list_elem;    /* List element */
};
