#include "vm/page.h"
#include "threads/vaddr.h"
#include "threads/palloc.h"
#include "threads/thread.h"
#include <hash.h>
#include <bitmap.h>

static void free_page (struct hash_elem *e, void *aux);

/* Returns a hash value for page p. */
static unsigned
page_hash (const struct hash_elem *p_, void *aux UNUSED)
{
  const struct pte *p = hash_entry (p_, struct pte, hash_elem);
  return hash_bytes (&p->addr, sizeof p->addr); 
}

/* Returns true if page a precedes page b. */
static bool
page_less (const struct hash_elem *a_, const struct hash_elem *b_,
           void *aux)
{
  const struct pte *a = hash_entry (a_, struct pte, hash_elem);
  const struct pte *b = hash_entry (b_, struct pte, hash_elem);

  return a->addr < b->addr;
}

 /* Returns the page containing the given virtual address,
   or a null pointer if no such page exists. */
struct pte *
page_lookup (const void *address, struct hash pages)
{
  struct pte *p = malloc(sizeof(struct pte *));
  struct hash_elem *e;

  p->addr = pg_round_down(address);
  e = hash_find (&pages, &p->hash_elem);

  return e != NULL ? hash_entry (e, struct pte, hash_elem) : NULL;
}

//struct hash *setup_pages()
void setup_pages(struct hash *pages)
{
  hash_init (pages, page_hash, page_less, NULL);
}

struct pte *add_page(struct hash *pages, void *upage, void *kpage, bool writable, bool swap)
{
  struct pte *page = malloc(sizeof(struct pte));;
  page->addr = upage;
  page->p_addr = vtop(kpage);
  page->swap_base = BITMAP_ERROR;
  page->writable = writable;
  page->swap = swap;
  page->file = NULL;
  page->read_bytes = 0;
  page->ofs = 0;
  hash_insert (pages, &page->hash_elem);
  return page;
}

void remove_all_pages(struct hash *pages)
{
  hash_destroy(pages, free_page);
}

/* Free each pte */
static void free_page
(struct hash_elem *e, void *aux)
{
  const struct pte *page = hash_entry (e, struct pte, hash_elem);
  free(page);
}
