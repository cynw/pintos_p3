#include <list.h>
#include "vm/frame.h"

struct ste {
  struct pte *page;				/* Pointer to its page */
  struct list_elem list_elem;			/* Hash element */
};
