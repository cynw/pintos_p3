#include "vm/swap.h"
#include "threads/malloc.h"
#include "threads/vaddr.h"
#include "threads/thread.h"
#include "threads/synch.h"
#include "threads/palloc.h"
#include "userprog/pagedir.h"
#include "devices/disk.h"
#include <list.h>
#include <bitmap.h>

#define SECTOR_PER_PAGE PGSIZE/DISK_SECTOR_SIZE

struct list slots_;
struct bitmap *slots;
struct lock swap_lock;

void setup_slots()
{
  list_init (&slots_);
  slots = bitmap_create(disk_size(disk_get(1, 1)));
  lock_init(&swap_lock);
}

/* Returns the slot of a page containing the given virtual address,
   or a null pointer if no such page exists. */
struct ste *
slot_lookup (const void *address)
{
  struct ste *ste;
  struct list_elem *e;

  lock_acquire(&swap_lock);
  for (e = list_begin (&slots_); e != list_end (&slots_); e = list_next (e)) {
    struct ste *s = list_entry (e, struct ste, list_elem);
    if (s->page->addr == address)
      ste = s;
  }
  lock_release(&swap_lock);

  return ste != NULL ? ste : NULL;
}

/* Swap in the given page to swap disk, evict its mapped frame from physical memory */
bool swap_in(struct fte *f)
{
  lock_acquire(&swap_lock);
  size_t idx = bitmap_scan_and_flip (slots, 0, SECTOR_PER_PAGE, false);
  lock_release(&swap_lock);

  struct disk *dsk = disk_get(1, 1);
  if (dsk == NULL) return false;

  size_t i;
  for(i = 0; i < SECTOR_PER_PAGE; i++) {
    disk_write(dsk, idx + i, f->page->addr + i * DISK_SECTOR_SIZE);
  }

  /* Update page table */
  f->page->location = PG_SWAP;
  f->page->swap_base = idx;
  pagedir_clear_page(thread_current()->pagedir, f->page->addr);

  /* Update frame table */
  list_remove(&f->list_elem);
  palloc_free_page(ptov(f->page->p_addr));
  free(f);
  
  return true;
}

/* Swap out the slot mapped by page that is referred by pte P 
   to FRAME_ADDR in physical memory. */
bool swap_out(struct pte *p)
{
  struct disk *dsk = disk_get(1, 1);
  if (dsk == NULL) return false;
  void *kpage = palloc_get_page(PAL_USER);
  
  ASSERT(kpage != NULL);
  
  pagedir_set_page(thread_current()->pagedir, p->addr, kpage, true);
  
  add_frame(p, vtop(kpage));
  
  size_t i;
  for(i = 0; i < SECTOR_PER_PAGE; i++) {
    disk_read(dsk, p->swap_base + i, kpage + (i * DISK_SECTOR_SIZE));
    lock_acquire(&swap_lock);
    bitmap_flip(slots, p->swap_base + i);
    lock_release(&swap_lock);
  }

  /* Update page table */
  p->location = PG_PHYMEM;
  p->p_addr = vtop(kpage);

  return true;  
}
