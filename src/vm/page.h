#include <hash.h>
#include "filesys/file.h"

enum pg_location {
  PG_PHYMEM,
  PG_SWAP,
  PG_FILE
};

struct pte {
  void *addr;                      /* Virtual address */
  uintptr_t p_addr;                /* Physical address */
  //void *p_addr;                    /* Physical address */
  size_t swap_base;                /* Mapping sector in swap disk */

  bool writable;                   /* Writable or not */
  bool swap;                       /* Canbe swapped out or not */

  enum pg_location location;       /* Page location */
  
  struct file *file;               /* If page is loaded from a file */
  uint32_t read_bytes;             /* Bytes to read from file */
  off_t ofs;                       /* Offset in file to read */
  
  struct hash_elem hash_elem;      /* Hash element */
};

void setup_pages(struct hash *pages);
struct pte *add_page(struct hash *, void *, void *, bool, bool);

